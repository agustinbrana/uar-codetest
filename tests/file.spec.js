import React from 'react';
import File from 'File';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

describe('File component suite', () => {

  it('should render without throwing an error', () => {
    expect(shallow(<File file={{name: 'sarasa'}} handleFileClick={function() { return false }}></File>).exists()).toBe(true)
  });

  it('renders a file icon', () => {
    expect(shallow(<File file={{name: 'sarasa'}} handleFileClick={function() { return false }}></File>).find('.file__icon').length).toEqual(1)
  })

  it('renders the file name', () => {
    expect(shallow(<File file={{name: 'sarasa'}} handleFileClick={function() { return false }}></File>).find('.file__name').length).toEqual(1)
  })

  it('renders correctly', () => {
    const tree = renderer.create(
      <File file={{name: 'sarasa'}} handleFileClick={function() { return true }}></File>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
