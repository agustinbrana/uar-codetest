import React, { Component } from 'react';
import axios from 'axios';
import TagFilters from './../components/TagFilters';
import Results from './../components/Results';

class Search extends Component {
  constructor() {
    super();
    this.state = {
      selectedTag: '',
      tags: [],
      files: [],
      currentPage: 1,
      filteredPages: 0,
    };
    this.handleFilterSelected = this.handleFilterSelected.bind(this);
    this.handleFileClick = this.handleFileClick.bind(this);
    this.getFilesByPage = this.getFilesByPage.bind(this);
  }

  componentDidMount() {
    this.getTags();
    this.getFilesByPage(1);
  }

  // Fetch Tags data from external API
  getTags() {
    document.body.classList.add('spinner');
    const url = 'http://tim.uardev.com/trial-project/api/tags';
    return axios.get(url).then(response => {
      document.body.classList.remove('spinner');
      this.setState({
        tags: response.data,
      });
    });
  }

  // Retrieves the files for a given page
  getFilesByPage(page) {
    document.body.classList.add('spinner');
    const baseUrl = `http://tim.uardev.com/trial-project/api/files?page=${page}`;
    const url = this.state.selectedTag ? `${baseUrl}&tag=${this.state.selectedTag}` : baseUrl;
    return axios.get(url).then(response => {
      document.body.classList.remove('spinner');
      this.setState({
        files: response.data.files,
        filteredPages: Math.ceil(response.data.total_files / 10),
        currentPage: page,
      });
    });
  }

  // Retrieves the files for a given tag
  handleFilterSelected(tag) {
    document.body.classList.add('spinner');
    const url = `http://tim.uardev.com/trial-project/api/files?page=1&tag=${tag}`;
    return axios.get(url).then(response => {
      document.body.classList.remove('spinner');
      this.setState({
        selectedTag: tag,
        files: response.data.files,
        filteredPages: Math.ceil(response.data.total_files / 10),
      });
    });
  }

  // Redirects to file detail page with selected file data
  handleFileClick(file) {
    this.props.history.push({
      pathname: `/file/${file.id}`,
      state: { file: file.name },
    });
  }

  render() {
    const { tags, files, filteredPages, selectedTag, currentPage } = this.state;
    return (
      <div className="search">
        <div className="search__filters">
          <TagFilters tags={tags} handleFilter={this.handleFilterSelected}></TagFilters>
        </div>
        <div className="search__results">
          <Results
            files={files}
            pages={filteredPages}
            selectedTag={selectedTag}
            currentPage={currentPage}
            handleFileClick={this.handleFileClick}
            handlePageSelect={this.getFilesByPage} 
          >
          </Results>
        </div>
      </div>
    );
  }
}

export default Search;
