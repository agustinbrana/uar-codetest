import React from 'react';
import Results from 'Results';
import File from 'File';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

let baseProps = {
  files: [{ id: 1, name: 'sarasa' }, { id: 2, name: 'sarasa' }],
  pages: 2,
  selectedTag: 'sarasa',
  currentPage: 1,
  handleFileClick: jest.fn(),
  handlePageSelect: jest.fn()
}

describe('Results component suite', () => {

  it('should render without throwing an error', () => {
    expect(shallow(<Results {...baseProps}></Results>).exists()).toBe(true);
  });

  it('renders pagination if pages are more than 1', () => {
    expect(shallow(<Results {...baseProps}></Results>).find('.results__pagination').length).toEqual(1);
  });

  it('doesnt renders pagination if pages are less than 2', () => {
    baseProps.pages = 1;
    baseProps.selectedTag = '';
    expect(shallow(<Results {...baseProps}></Results>).find('.results__pagination').length).toEqual(0);
  });

  it('renders a list of files', () => {
    const component = enzyme.mount(<Results {...baseProps}></Results>);
    expect(component.find(File)).toHaveLength(2);
  });

  it('renders correctly', () => {
    const tree = renderer.create(
      <Results {...baseProps}></Results>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
