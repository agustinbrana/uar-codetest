import React, { Component } from 'react';

class TagFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filtersDisplayed: false,
    };
    this.toggleTagFilters = this.toggleTagFilters.bind(this);
  }

  // Toggles filtersDisplayed state in order to show/hide filters sidebar
  toggleTagFilters() {
    const { filtersDisplayed } = this.state;
    this.setState({ filtersDisplayed: !filtersDisplayed });
  }

  handleFilterSelected(tag) {
    this.toggleTagFilters();
    this.props.handleFilter(tag.tag);
  }

  render() {
    return (
      <div className={`tag-filters ${this.state.filtersDisplayed ? 'tag-filters--open' : ''}`}>
        <div className="tag-filters__container">
          <h3 className="tag-filters__heading">TAGS</h3>
          <div className="tag-filters__control" onClick={this.toggleTagFilters}></div>
          {this.props.tags.map((tag, i) => {
            return (
              <div className="tag-filters__filter" onClick={this.handleFilterSelected.bind(this,tag)} key={i}>
                <span>{tag.tag}</span>
                <span> ({tag.files})</span>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default TagFilters;
