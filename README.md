# Up & Running - Code Test
## Candidate: Agustin Braña Figueroa

#### To run locally
- `npm install`
- `npm run start`

#### To run build
- `npm run build`

#### To run tests
- `npm run test`

#### Coverage Report
Can be found in folder coverage/lcov-report/index.html

#### SSR 
I've left inside the server folder (server/server.js, server/index.js) the setup necessary for SSR.
We would need to change the configuration in .babelrc and change BrowserRouter on the client side in order use SSR.

#### Time spent building the project
- 15hs