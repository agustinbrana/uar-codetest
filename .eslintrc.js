const path = require('path');

module.exports = {
  extends: 'airbnb',
  plugins: [
    'react',
    'jsx-a11y',
    'import'
  ],
  env: {
    'browser': true,
    'jest': true
  },
  parser: 'babel-eslint',
  parserOptions: {
    'sourceType': 'module',
    'allowImportExportEverywhere': true
  },
  rules: {
    'no-param-reassign': 'off',
    'prefer-rest-params': 'off',
    'no-console': 'off',
    'no-plusplus': 'off',
    'no-lonely-if': 'off',
    'arrow-body-style': 'off',
    'no-mixed-operators': 'off',
    'arrow-parens': 'off',
    'radix': 'off',
    'array-callback-return': 'off',
    'object-curly-newline': 'off',
    'no-case-declarations': 'off',
    'function-paren-newline': 'off',
    'prefer-destructuring': [
      'error',
      {
        'object': true,
        'array': false
      }
    ],
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'warn',
    'react/forbid-prop-types': 'off',
    'react/jsx-boolean-value': 'off',
    'react/prefer-stateless-function': 'off',
    'react/self-closing-comp': 'off',
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    'jsx-a11y/label-has-for': [
      2,
      {
        'components': [
          'Label'
        ],
        'required': {
          'every': [
            'id'
          ]
        },
        'allowChildren': false
      }
    ]
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: path.resolve(__dirname, '..', '..', 'build-tools', 'react', 'webpack.common.js'),
      }
    }
  },
  'root': true
};
