import React from 'react';
import TagFilters from 'TagFilters';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

describe('TagFilters component suite', () => { 
  it('should render without throwing an error', () => {
    expect(shallow(
      <TagFilters 
        tags={['tag1', 'tag2']} 
        handleFilter={() => true}>
      </TagFilters>
    ).exists()).toBe(true);
  });

  it('renders a list of filters', () => {
    expect(shallow(
      <TagFilters
        tags={['tag1', 'tag2']} 
        handleFilter={() => true}
      >
      </TagFilters>
    ).find('.tag-filters__filter').length).toEqual(2);
  });

  it('renders correctly', () => {
    const tree = renderer.create(
      <TagFilters 
        tags={['tag1', 'tag2']} 
        handleFilter={() => true}>
      </TagFilters>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('handleFilterSelected', () => {
    it('Should call toggleTagFilters', () => {
      const baseProps = {
        tags: ['tag1', 'tag2'],
        handleFilter: jest.fn(),
      };
      const component = enzyme.mount(<TagFilters {...baseProps}></TagFilters>);
      component.instance().toggleTagFilters = jest.fn();
      component.instance().handleFilterSelected({ tag: 'tag1' });
      component.update();
      expect(component.instance().toggleTagFilters.mock.calls).toHaveLength(1);
      expect(baseProps.handleFilter.mock.calls).toHaveLength(1);
    });
  });

  describe('toggleTagFilters', () => {
    it('Should toggle filtersDisplayed state value', () => {
      const component = enzyme.mount(<TagFilters tags={['tag1', 'tag2']} handleFilter={() => true}></TagFilters>);
      expect(component.state().filtersDisplayed).toEqual(false);
      component.instance().toggleTagFilters();
      component.update();
      expect(component.state().filtersDisplayed).toEqual(true);
    });
  });
});
