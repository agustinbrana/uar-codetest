
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import InputText from '../components/InputText';

class FileDetail extends Component {
  constructor() {
    super();
    this.state = {
      input: '',
      invalidInput: false,
    };
    this.handleRenameFile = this.handleRenameFile.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // Show the warning message if the input is empty or if not post the new file name
  handleRenameFile() {
    const inputValue = this.state.input;
    if (inputValue.length < 1) {
      this.setState({ invalidInput: true });
      return;
    }
    document.body.classList.add('spinner');
    const url = `http://tim.uardev.com/trial-project/api/file/${this.props.match.params.id}/rename`;
    const newName = { filename: inputValue };
    axios.post(url, newName).then(() => {
      document.body.classList.remove('spinner');
      this.props.history.push({
        pathname: '/',
      });
    }).catch(error => {
      // Handle error.
      console.error('An error occurred while changing the file name', error);
    });
  }

  // Update the state with whatever the user types in the input
  handleInputChange(e) {
    const inputValue = e.target.value;
    if (inputValue.length > 1) {
      this.setState({ invalidInput: false });
    }
    this.setState({ input: inputValue });
  }

  render() {
    const { invalidInput } = this.state;
    return (
      <div className="file-detail">
        <h1 className="file-detail__heading">Rename File</h1>
        <InputText
          file={this.props.location.state.file}
          handleInputChange={this.handleInputChange}
          placeholder="Enter the new file name"
          invalidInput={invalidInput}
          validationMessage="You must enter a value for the new file name"
        >
        </InputText>
        <div className="file-detail__actions">
          <button className="button button--primary" onClick={this.handleRenameFile}>Save</button>
          <Link className="file-detail__back-button" to="/" href="/">
            <button className="button button--secondary">Back to File List</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default FileDetail;
