module.exports = {
    verbose: true,
    moduleDirectories: ["node_modules", "src/components", "src/pages" ],
    testRegex: "(/tests/.*|(\\.|/)spec)\\.js$"
};