const path = require("path");
const merge = require("webpack-merge");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const CleanWbepackPlugin = require("clean-webpack-plugin");

const webpackCommon = require("./webpack.common");

const pathsToClean = ["build"];
const cleanOptions = {
  root: path.resolve(__dirname, "../")
};

module.exports = merge.smart(webpackCommon, {
  output: {
    filename: "main.[chunkhash].js",
    path: path.resolve(__dirname, "../build"),
    publicPath: "/"
  },
  mode: "production",
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          warnings: false,
          parse: {},
          compress: {},
          mangle: true, // Note `mangle.properties` is `false` by default.
          output: null,
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_fnames: false,
        }
      })
    ],
    runtimeChunk: true,
  },
  plugins: [
    new CleanWbepackPlugin(pathsToClean, cleanOptions),
    new OptimizeCssAssetsPlugin()
  ]
});
