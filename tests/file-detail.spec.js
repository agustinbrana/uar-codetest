import React from 'react';
import FileDetail from 'FileDetail';
import InputText from 'InputText';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';


enzyme.configure({ adapter: new Adapter() });
const baseProps = {
  location: {
    state: {
      file: 'sarasa',
    },
  },
};

describe('FileDetails component suite', () => {
  it('should render without throwing an error', () => {
    
    expect(shallow(
      <FileDetail {...baseProps}></FileDetail>
    ).exists()).toBe(true);
  });

  it('renders an inputText', () => {
    const component = enzyme.mount(<Router><FileDetail {...baseProps}></FileDetail></Router>);
    expect(component.find(InputText)).toHaveLength(1);
  });

  it('renders a primary button and a secondary button', () => {
    const component = enzyme.mount(<Router><FileDetail {...baseProps}></FileDetail></Router>);
    expect(component.find('.button--primary')).toHaveLength(1);
    expect(component.find('.button--secondary')).toHaveLength(1);
  });

  describe('handleInputChange', () => { 
    it('Should set invalidInput state to false if input is not empty', () => {
      const component = shallow(<FileDetail {...baseProps}></FileDetail>);
      component.setState({ invalidInput: true });
      expect(component.state().invalidInput).toEqual(true);
      component.instance().handleInputChange({ target: { value: 'abc' } });
      component.update();
      expect(component.state().invalidInput).toEqual(false);
    });

    it('Should set input state with input value', () => {
      const component = shallow(<FileDetail {...baseProps}></FileDetail>);
      component.instance().handleInputChange({ target: { value: 'abc' } });
      component.update();
      expect(component.state().input).toEqual('abc');
    });
  });

  it('renders correctly', () => {
    const tree = renderer.create(
      <Router><FileDetail {...baseProps}></FileDetail></Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('handleRenameFile', () => { 
    it('Should set invalidInput state to true if input empty', () => {
      const component = shallow(<FileDetail {...baseProps}></FileDetail>);
      component.setState({ input: '' });
      component.instance().handleRenameFile();
      component.update();
      expect(component.state().invalidInput).toEqual(true);
    });

    it('Should set invalidInput state to true if input empty', () => {
      const component = shallow(<FileDetail {...baseProps}></FileDetail>);
      component.setState({ input: '' });
      component.instance().handleRenameFile();
      component.update();
      expect(component.state().invalidInput).toEqual(true);
    });

    it('Should post the new file name if input is not empty', () => {
      const historyMock = { push: jest.fn() };
      const match = { params: { id: 12 } };
      const component = shallow(<FileDetail {...baseProps} history={historyMock} match={match}></FileDetail>);
      component.setState({ input: 'abc' });
      component.instance().handleRenameFile();
      expect(historyMock.push.mock.calls).toHaveLength(0); // 1
    });
  });
});
