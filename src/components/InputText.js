import React from 'react';

const InputText = (props) => {
  const { file, invalidInput, placeholder, handleInputChange, validationMessage } = props;
  return (
    <div className="input-text">
      <span className="input-text__label">{file}</span>
      <input
        className={`input-text__input ${invalidInput ? 'input-text__input--invalid' : ''}`}
        type="text" 
        placeholder={placeholder}
        onChange={handleInputChange}
      >
      </input>
      {invalidInput && <div className="input-text__warning">{validationMessage}</div>}
    </div>
  );
};

module.exports = InputText;
