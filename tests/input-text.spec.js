
import React from 'react';
import InputText from 'InputText';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

describe('InputText component suite', () => {
  it('should render without throwing an error', () => {
    expect(shallow(
      <InputText 
      file='sarasa'
      invalidInput={false} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      ></InputText>
    ).exists()).toBe(true);
  });

  it('renders an input label', () => {
    expect(shallow(
      <InputText 
      file='sarasa'
      invalidInput={false} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      ></InputText>
    ).find('.input-text__label').length).toEqual(1)
  });

  it('renders an input', () => {
    expect(shallow(
      <InputText 
      file='sarasa'
      invalidInput={false} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      ></InputText>
    ).find('.input-text__input').length).toEqual(1)
  });

  it('renders a warning message if invalid input', () => {
    expect(shallow(
      <InputText 
      file='sarasa'
      invalidInput={true} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      ></InputText>
    ).find('.input-text__warning').length).toEqual(1)
  });

  it('doesnt render a warning message if valid input', () => {
    expect(shallow(
      <InputText 
      file='sarasa'
      invalidInput={false} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      ></InputText>
    ).find('.input-text__warning').length).toEqual(0)
  });

  it('renders correctly', () => {
    const tree = renderer.create(
      <InputText 
      file='sarasa'
      invalidInput={false} 
      handleInputChange={function() { return true }}
      placeholder={'some placeholder'}
      validationMessage={'message'}
      >
    </InputText>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
