import React from 'react';
import File from './File';

const Results = (props) => {
  const { handleFileClick, selectedTag, pages, handlePageSelect, currentPage } = props;
  const files = props.files.map(file => {
    return (
      <File key={file.id} file={file} handleFileClick={handleFileClick}></File>
    );
  });
  const searchSelection = selectedTag ? `${selectedTag} Tag` : 'No tag selected';
  return (
    <div className="results">
      <h1 className="results__heading">Search Results - {searchSelection}</h1>
      <div className="results__files-container">{files}</div>
      {pages > 1 &&
        <div className="results__pagination">
          {
            new Array(pages).fill(0).map((n, i) => {
              return (
                <span
                  key={i}
                  className={`results__page-number ${currentPage === i+1 ? 'results__page-number--selected' : '' } `}
                  onClick={handlePageSelect.bind(this, i+1)}
                >
                  { i + 1 }
                </span>
              );
            })
          }
        </div>
      }
    </div>
  );
};

module.exports = Results;
