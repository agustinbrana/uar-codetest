import React from 'react';
import Search from 'Search';
import TagFilters from 'TagFilters';
import Results from 'Results';
import renderer from 'react-test-renderer';
import enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';

enzyme.configure({ adapter: new Adapter() });
jest.mock('axios');
axios.get.mockImplementation(() => Promise.resolve({ data: { files: ['file1'], total_files: 10, tags: ['tag1'] } }));

describe('Search component suite', () => {
  it('should render without throwing an error', () => {
    expect(shallow(
      <Search />
    ).exists()).toBe(true);
  });

  it('renders the TagFilters component', () => {
    const component = shallow(<Search />);
    expect(component.find(TagFilters)).toHaveLength(1);
  });

  it('renders the Search component', () => {
    const component = shallow(<Search />);
    expect(component.find(Results)).toHaveLength(1);
  });

  it('renders correctly', () => {
    const tree = renderer.create(
      <Search />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('handleFileClick', () => { 
    it('Should history push to file route path', () => {
      const historyMock = { push: jest.fn() };
      const component = shallow(<Search history={historyMock} ></Search>);
      component.instance().handleFileClick({ id: 12 });
      expect(historyMock.push.mock.calls).toHaveLength(1);
    });
  });

  describe('handleFilterSelected', () => { 
    it('Should retrieve files for the selected tag and set selectedTag, files and filteredPages states', () => {
      const component = shallow(<Search />);
      return component.instance().handleFilterSelected('Test1').then(() => {
        expect(component.state().selectedTag).toEqual('Test1');
        expect(component.state().files).toEqual(['file1']);
        expect(component.state().filteredPages).toEqual(1);
      });
    });
  });

  describe('getFilesByPage', () => { 
    it('Should retrieve files for the selected and set files, filteredPages and currentPage states', () => {
      const component = shallow(<Search />);
      return component.instance().getFilesByPage(1).then(() => {
        expect(component.state().files).toEqual(['file1']);
        expect(component.state().filteredPages).toEqual(1);
        expect(component.state().currentPage).toEqual(1);
      });
    });
  });

  describe('getTags', () => { 
    it('Should retrieve tags and set tags state', () => {
      axios.get.mockImplementation(() => Promise.resolve({ data: ['tag1'] }));
      const component = shallow(<Search />);
      return component.instance().getTags().then(() => {
        expect(component.state().tags).toEqual(['tag1']);
      });
    });
  });

});
