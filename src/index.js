import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Search from './pages/Search';
import FileDetail from './pages/FileDetail';
import './styles/style.scss';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Route
          exact
          path="/"
          render={({ history }) => (
            <Search
              history={history}
            />
          )}
        />
        <div className="loading-container">
          <div className="loading"></div>
        </div>
        <Route path="/file/:id" component={FileDetail} />
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
