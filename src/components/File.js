import React from 'react';

const File = (props) => {
  const file = props.file;
  return (
    <div className="file" onClick={props.handleFileClick.bind(this, file)}>
      <div className="file__icon"></div>
      <div className="file__name">{file.name}</div>
    </div>
  );
};

module.exports = File;
